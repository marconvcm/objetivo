//
//  AppCoordinator.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator {

    let window: UIWindow
    let rootContainer: Root
    var modules: [ModuleInterface] = []
    
    lazy var navigationViewController: UINavigationController = {
        let viewController  = UINavigationController(rootViewController: UIViewController())
        return viewController
    }()

    init(_ window: UIWindow, rootContainer: Root) {
        self.rootContainer = rootContainer
        self.window = window
        self.window.rootViewController = navigationViewController
        self.window.makeKeyAndVisible()
    }
    
    func start() {
        let module = GoalsModule(
            repository: self.rootContainer.container.resolve(ActivityRepository.self)!
        )
        presentAsRootModule(module)
    }
    
    func presentModule(_ module: ModuleInterface) {
        let viewController = module.wireframe(coordinator: self)
        self.navigationViewController.pushViewController(viewController, animated: true)
        modules.append(module)
    }
    
    func presentAsRootModule(_ module: ModuleInterface) {
        let viewController = module.wireframe(coordinator: self)
        self.navigationViewController.viewControllers = [viewController]
        modules = [module]
    }
}
