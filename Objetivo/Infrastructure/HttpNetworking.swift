//
//  HttpNetworking.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class HttpNetworking: Networking {

    func GET(of url: String) -> Observable<NetworkingResponse> {
        return Observable.create { observer in
            Alamofire.request(url).responseJSON { response in
                let response = NetworkingResponse(
                    request: response.request,
                    response: response.response,
                    data: response.data,
                    error: response.error
                )
                observer.on(.next(response))
                observer.on(.completed)
            }
            return Disposables.create()
        }
    }
}
