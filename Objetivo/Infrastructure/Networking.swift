//
//  Networking.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

protocol Networking {
    
    func GET(of url: String) -> Observable<NetworkingResponse>
}
