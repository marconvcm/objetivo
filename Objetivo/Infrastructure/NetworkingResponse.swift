//
//  NetwokingResponse.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation

struct NetworkingResponse {
    
    /// The URL request sent to the server.
    let request: URLRequest?
    
    /// The server's response to the URL request.
    let response: HTTPURLResponse?
    
    /// The data returned by the server.
    let data: Data?
    
    /// The error encountered while executing or validating the request.
    let error: Error?
}


