//
//  HealthKitActivityRepository.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift

class HealthKitActivityRepository: ActivityRepository {
    
    let goalRepository: GoalRepository
    let stepService: StepService
    let distanceService: DistanceService
    
    init(goalRepository: GoalRepository, stepService: StepService, distanceService: DistanceService) {
        self.goalRepository = goalRepository
        self.stepService = stepService
        self.distanceService = distanceService
    }
    
    func fetch() -> Observable<[Activity]> {
        return self.stepService.requestStepCount().flatMap { steps in
            return self.distanceService.requestWalkedDistance().flatMap { distance in
                return Observable.of(
                    ActivitySample(
                        steps: steps,
                        distance: distance
                    )
                )
            }
        }.flatMap { sample in
            return self.goalRepository.fetch().map { goals in
                return goals.map { goal in
                    Activity(
                        steps: sample.steps,
                        running: sample.distance,
                        goal: goal
                    )
                }
            }
        }
    }
}

fileprivate struct ActivitySample {
    let steps: Double
    let distance: Double
}
