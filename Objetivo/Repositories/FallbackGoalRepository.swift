//
//  FallbackGoalRepository.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift

class FallbackGoalRepository: GoalRepository {
    
    let localGoalService: GoalStorageService
    let remoteGoalService: GoalService
    
    init(localGoalService: GoalStorageService, remoteGoalService: GoalService) {
        self.localGoalService = localGoalService
        self.remoteGoalService = remoteGoalService
    }
    
    func fetch() -> Observable<[Goal]> {
        return self.remoteGoalService.fetch()
            .flatMap { $0.isEmpty ? self.localGoalService.fetch() : Observable.of($0) }
            .flatMap { self.localGoalService.save(goals: $0) }
    }
}
