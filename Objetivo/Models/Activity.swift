//
//  Activity.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation

struct Activity {
    let steps: Double
    let running: Double
    let goal: Goal
    
    var title: String {
        return goal.title
    }
    
    var subtitle: String {
        return goal.description
    }
    
    var type: String {
        return goal.type.rawValue
    }
    
    var scoreboard: String {
        switch goal.type {
        case .runningDistance, .walkingDistance:
            return "\(running)m/\(goal.goal/1000)km"
        case .step:
            return "\(steps)/\(goal.goal)"
        }
    }
    
    var completed: Bool {
        switch goal.type {
        case .runningDistance, .walkingDistance:
            return Int(running) >= goal.goal
        case .step:
            return Int(steps) >= goal.goal 
        }
    }
}
