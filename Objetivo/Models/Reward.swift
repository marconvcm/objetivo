//
//  Reward.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation

struct Reward: Codable {
    let trophy: RewardTrophy
    let points: Int
}

enum RewardTrophy:  String, Codable {
    case goldMedal = "gold_medal"
    case silverMedal = "silver_medal"
    case bronzeMedal = "bronze_medal"
    case zombieHand = "zombie_hand"
}
