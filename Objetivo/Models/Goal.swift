//
//  Goals.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//
// Sample Data
// {
//    "id": "1004",
//    "title": "Quick Run",
//    "description": "Burn that donut by running 1 kilometer",
//    "type": "running_distance",
//    "goal": 1000,
//    "reward": {
//        "trophy": "silver_medal",
//        "points": 5
//    }
// },

import Foundation

struct Goal: Codable {
    let id: String
    let title: String
    let description: String
    let type: GoalType
    let goal: Int
    let reward: Reward
}

enum GoalType: String, Codable {
    case runningDistance = "running_distance"
    case walkingDistance = "walking_distance"
    case step = "step"
}
