//
//  UICollectionView+Extension.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 03/05/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    var isReachingEnd: Bool {
        let height = frame.size.height
        let contentYoffset = contentOffset.y
        let distanceFromBottom = contentSize.height - contentYoffset
        return distanceFromBottom < height
    }
}
