//
//  GoalsViewController.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class GoalsViewController: UIViewController, UICollectionViewDelegateFlowLayout, GoalsViewInterface {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var refreshControl: UIRefreshControl!
    
    var disposeBag = DisposeBag()
    var presenter: GoalsPresenterInterface!
    
    override func viewDidLoad() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(reload(_:)), for: .valueChanged)
        collectionView.delegate = self
        collectionView.refreshControl = refreshControl
        self.collectionView.rx.modelSelected(Activity.self).subscribe(self.didModelSelected).disposed(by: disposeBag)
        self.reload(self.refreshControl)
    }
    
    @objc func reload(_ sender: Any) {
        presenter.requestDataLoad()
    }
    
    func renderData(with observable: Observable<[Activity]>) {
        collectionView.dataSource = nil
        observable.bind(
            to: self.collectionView.rx.items(cellIdentifier: "GoalsCollectionViewCell", cellType: GoalsCollectionViewCell.self)
        ) { row, data, cell in
            cell.model = data
        }.disposed(by: disposeBag)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        refreshControl.endRefreshing()
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(
            width: collectionView.bounds.width,
            height: 150
        )
    }
    
    func didModelSelected(event: Event<Activity>) {
        guard let activity = event.element else { return }
        self.presenter.requestAccess(to: activity)
    }
}

class GoalsCollectionViewCell: UICollectionViewCell {

    var model: Activity! {
        didSet {
            titleLabel.text = model.title
            subtitleLabel.text = model.subtitle
            goalLabel.text = model.scoreboard
            typeImage.image = UIImage(named: model.type)
        }
    }
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var subtitleLabel: UILabel!
    
    @IBOutlet var goalLabel: UILabel!
    
    @IBOutlet var typeImage: UIImageView!
}
