//
//  GoalsPresenter.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class GoalsPresenter: GoalsPresenterInterface {
    
    var module: ModuleInterface?
    let view: GoalsViewInterface
    let repository: ActivityRepository
    
    required init(view: GoalsViewInterface, repository: ActivityRepository) {
        self.view = view
        self.repository = repository
    }
    
    func requestDataLoad() {
        let data = self.repository.fetch()
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribeOn(MainScheduler.instance)
        self.view.renderData(with: data)
    }
    
    func requestAccess(to activity: Activity) {
        let m = self.module as! GoalsModuleInterface
        m.showActivityDetails(activity: activity)
    }
}
