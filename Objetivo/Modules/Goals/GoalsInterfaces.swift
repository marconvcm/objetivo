//
//  GoalInterfaces.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift

protocol GoalsViewInterface {
    
    var presenter: GoalsPresenterInterface! { get }
    func renderData(with observable: Observable<[Activity]>)
}

protocol GoalsPresenterInterface: PresenterInterface {
    init(view: GoalsViewInterface, repository: ActivityRepository)
    func requestDataLoad()
    func requestAccess(to activity: Activity)
}

protocol GoalsModuleInterface: ModuleInterface {
    init(repository: ActivityRepository)
    func showActivityDetails(activity: Activity)
}

