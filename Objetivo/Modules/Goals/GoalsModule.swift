//
//  GoalsModule.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import UIKit

class GoalsModule: GoalsModuleInterface {

    var repository: ActivityRepository
    var coordinator: AppCoordinator?
    
    required init(repository: ActivityRepository) {
        self.repository = repository
    }
    
    func wireframe(coordinator: AppCoordinator) -> UIViewController {
        self.coordinator = coordinator
        
        let viewController = UIStoryboard(
            name: "Goals",
            bundle: nil
        ).instantiateViewController(
            withIdentifier: "GoalsViewController"
        ) as! GoalsViewController
        
        let presenter = GoalsPresenter(view: viewController, repository: self.repository)
        presenter.module = self
        viewController.presenter = presenter

        return viewController
    }
    
    func showActivityDetails(activity: Activity) {
        let module = RewardModule(activity: activity)
        coordinator?.presentModule(module)
    }
}
