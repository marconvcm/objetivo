//
//  RewardModule.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import UIKit

class RewardModule: RewardModuleInterface {

    var activity: Activity!
    var coordinator: AppCoordinator?
    
    required init(activity: Activity) {
        self.activity = activity
    }
    
    func wireframe(coordinator: AppCoordinator) -> UIViewController {
        self.coordinator = coordinator
        
        let viewController = UIStoryboard(
            name: "Reward",
            bundle: nil
            ).instantiateViewController(
                withIdentifier: "RewardViewController"
        ) as! RewardViewController
        
        let presenter = RewardPresenter(view: viewController, activity: self.activity)
        presenter.module = self
        viewController.presenter = presenter
        
        return viewController
    }
}
