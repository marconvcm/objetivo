//
//  RewardViewController.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import UIKit

class RewardViewController: UIViewController, RewardViewInterface {
    
    @IBOutlet var rewardBadgeImageView: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var descriptionLabel: UILabel!
    
    @IBOutlet var pointsLabel: UILabel!
    
    @IBOutlet var scoreboardLabel: UILabel!
    
    var presenter: RewardPresenterInterface!
    
    var activity: Activity? {
        didSet {
            guard let activity = self.activity else { return }
            rewardBadgeImageView.image = UIImage(named: activity.goal.reward.trophy.rawValue)
            rewardBadgeImageView.alpha = activity.completed ? 1.0 : 0.05
            titleLabel.text = activity.title
            descriptionLabel.text = activity.subtitle
            pointsLabel.text = activity.completed ? "\(activity.goal.reward.points) points" : "moving on!"
            scoreboardLabel.text = activity.scoreboard
        }
    }
    
    override func viewDidLoad() {
        self.presenter.requestDataLoad()
    }
    
    func renderData(with activity: Activity) {
        self.activity = activity
    }
}
