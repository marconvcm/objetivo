//
//  RewardPresenter.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation

class RewardPresenter: RewardPresenterInterface {
    
    let activity: Activity
    let view: RewardViewInterface
    var module: ModuleInterface?
    
    required init(view: RewardViewInterface, activity: Activity) {
        self.activity = activity
        self.view = view
    }
    
    func requestDataLoad() {
        self.view.renderData(with: self.activity)
    }
}
