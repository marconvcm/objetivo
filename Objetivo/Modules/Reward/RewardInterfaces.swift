//
//  RewardInterfaces.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift

protocol RewardViewInterface {
    var presenter: RewardPresenterInterface! { get }
    func renderData(with activity: Activity)
}

protocol RewardPresenterInterface: PresenterInterface {
    init(view: RewardViewInterface, activity: Activity)
    func requestDataLoad()
}

protocol RewardModuleInterface: ModuleInterface {
    init(activity: Activity)
}

