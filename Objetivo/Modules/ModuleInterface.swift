//
//  Module.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import UIKit

protocol ModuleInterface {
 
    var coordinator: AppCoordinator? { get }
    
    func wireframe(coordinator: AppCoordinator) -> UIViewController
}
