//
//  PresenterInterface.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation

protocol PresenterInterface {
    var module: ModuleInterface? { get }
}
