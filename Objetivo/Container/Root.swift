//
//  Container.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import Swinject

class Root {
    lazy var container: Container = {
        
        let container = Container()
        
        container.register(String.self, name: "baseApiUrl") { _ in
            return "https://wellness9.htmind.com.br/api/v1"
        }
        
        container.register(Networking.self) { _ in
            return HttpNetworking()
        }
        
        container.register(GoalService.self) { (r: Resolver) -> GoalService in
            return RemoteGoalService (
                networking: r.resolve(Networking.self)!,
                baseApiUrl: r.resolve(String.self, name: "baseApiUrl")!
            )	
        }
        
        container.register(GoalStorageService.self) { _ in
            return LocalGoalService()
        }
        
        container.register(StepService.self) { _ in
            return HealthKitStepService()
        }
        
        container.register(DistanceService.self) { _ in
            return HealthKitDistanceService()
        }
        
        container.register(GoalRepository.self) { (r: Resolver) -> FallbackGoalRepository in
            FallbackGoalRepository(
                localGoalService: r.resolve(GoalStorageService.self)!,
                remoteGoalService: r.resolve(GoalService.self)!
            )
        }
        
        container.register(ActivityRepository.self) { (r: Resolver) -> ActivityRepository in
            HealthKitActivityRepository(
                goalRepository: r.resolve(GoalRepository.self)!,
                stepService: r.resolve(StepService.self)!,
                distanceService: r.resolve(DistanceService.self)!
            )
        }
        
        return container
    }()
}
