//
//  SteapService.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift

protocol StepService {
    
    func requestStepCount() -> Observable<Double>
}
