//
//  GoalStorageService.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift

protocol GoalStorageService: GoalService {
    
    func save(goals: [Goal]) -> Observable<[Goal]>
}
