//
//  RemoteGoalService.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift

class RemoteGoalService: GoalService {
    
    let decoder = JSONDecoder()
    let networking: Networking
    let baseApiUrl: String
    
    init(networking: Networking, baseApiUrl: String) {
        self.networking = networking
        self.baseApiUrl = baseApiUrl
    }
    
    func fetch() -> Observable<[Goal]> {
        return networking.GET(of: "\(baseApiUrl)/goals").map { response in
            return (try? self.decoder.decode(
                [Goal].self,
                from: response.data ?? Data()
            )) ?? []
        }
    }
}
