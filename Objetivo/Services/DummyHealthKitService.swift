//
//  DummyHealthKitService.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift

class DummyHealthKitService: StepService, DistanceService {
    
    func requestWalkedDistance() -> Observable<Double> {
        return Observable.of(100)
    }
    
    func requestStepCount() -> Observable<Double>  {
        return Observable.of(400)
    }
}
