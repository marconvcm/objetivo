//
//  LocalGoalService.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift
import Cache

class LocalGoalService: GoalStorageService {
 
    static let KEY = "LocalGoalService.data"
    
    let storage: Storage<[Goal]>
    
    init() {
        let diskConfig = DiskConfig(name: "LocalGoalService")
        let memoryConfig = MemoryConfig(expiry: .never, countLimit: 10, totalCostLimit: 10)
        storage = try! Storage(
            diskConfig: diskConfig,
            memoryConfig: memoryConfig,
            transformer: TransformerFactory.forCodable(ofType: [Goal].self)
        )
    }
    
    func fetch() -> Observable<[Goal]> {
        let result = (try? storage.object(forKey: LocalGoalService.KEY))
        return Observable.of(result ?? [])
    }
    
    func save(goals: [Goal]) -> Observable<[Goal]> {
        try? storage.setObject(goals, forKey: LocalGoalService.KEY)
        return fetch()
    }
}
