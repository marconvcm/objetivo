//
//  HealthKitStepService.swift
//  Objetivo
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import Foundation
import RxSwift
import HealthKit

class HealthKitStepService: StepService {
    
    let sampleTypes = Set([
        HKObjectType.quantityType(forIdentifier: .stepCount)!,
    ])
    
    let healthStore = HKHealthStore()
    
    func requestStepCount() -> Observable<Double> {
        
        return Observable.create { observer in
            self.healthStore.requestAuthorization(toShare: [], read: self.sampleTypes) { sucess, error in
                let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
                let now = Date()
                let startOfDay = Calendar.current.startOfDay(for: now)
                let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
                
                let query = HKStatisticsQuery(
                    quantityType: stepsQuantityType,
                    quantitySamplePredicate: predicate,
                    options: .cumulativeSum) { _, result, _ in
                        guard
                            let result = result,
                            let sum = result.sumQuantity() else {
                                observer.on(.next(0.0))
                                observer.on(.completed)
                                return
                        }
                        observer.on(.next(sum.doubleValue(for: HKUnit.count())))
                        observer.on(.completed)
                }
                self.healthStore.execute(query)
            }
            return Disposables.create()
        }
    }
}
