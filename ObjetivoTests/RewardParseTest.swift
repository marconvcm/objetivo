//
//  RewardParseTest.swift
//  ObjetivoTests
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import XCTest

@testable import Objetivo

class RewardParseTest: XCTestCase {

    var json: String!
    
    var expected: Reward = Reward(trophy: .silverMedal, points: 5)
    
    override func setUp() {
        json = """
        {
            "trophy": "silver_medal",
            "points": 5
        }
        """
    }
    
    func testParse() {
        let parseResult = try! JSONDecoder().decode(Reward.self, from: json.data(using: .utf8)!)
        XCTAssertEqual(expected.trophy, parseResult.trophy)
        XCTAssertEqual(expected.points, parseResult.points)
    }
}
