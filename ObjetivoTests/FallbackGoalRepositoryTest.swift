//
//  FallbackGoalRepositoryTest.swift
//  ObjetivoTests
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import XCTest
import RxTest
import RxSwift
import RxBlocking
@testable import Objetivo


class FallbackGoalRepositoryTest: XCTestCase {

    var localService: GoalStorageService!
    var remoteService: GoalService!
    var remoteOfflineService: GoalService!
    var repository: FallbackGoalRepository!
    
    override func setUp() {
        remoteService = MockRemoteService()
        remoteOfflineService = MockOfflineRemoteService()
        localService = MockLocalService()
    }

    override func tearDown() {
        repository = nil
        remoteService = nil
        remoteOfflineService = nil
        localService = nil
    }

    func testFetchOnline() {
        do {
            repository = FallbackGoalRepository(localGoalService: localService, remoteGoalService: remoteService)
            let result = try repository.fetch().toBlocking().first()
            XCTAssert(result?.count == 2)
        } catch { }
    }
    
    func testFetchOffline() {
        do {
            repository = FallbackGoalRepository(localGoalService: localService, remoteGoalService: remoteService)
            let _ = try repository.fetch().toBlocking().first()
            
            repository = FallbackGoalRepository(localGoalService: localService, remoteGoalService: remoteOfflineService)
            let result = try repository.fetch().toBlocking().first()
            
            XCTAssert(result?.count == 2)
        } catch { }
    }
}

fileprivate class MockOfflineNetworking: Networking {
    
    func GET(of url: String) -> Observable<NetworkingResponse> {
        return Observable.of(NetworkingResponse(request: nil, response: nil, data: nil, error: nil))
    }
}

class MockLocalService: GoalStorageService {
    
    var goals: [Goal]?
    
    func fetch() -> Observable<[Goal]> {
        return Observable.of(goals ?? [])
    }
    
    func save(goals: [Goal]) -> Observable<[Goal]> {
        self.goals = goals
        return fetch()
    }
}

class MockOfflineRemoteService: GoalService {
    
    func fetch() -> Observable<[Goal]> {
        return Observable.of([])
    }
}

class MockRemoteService: GoalService {
    
    func fetch() -> Observable<[Goal]> {
        return Observable.of([
            Goal(
                id: "1004",
                title: "Quick Run",
                description: "Burn that donut by running 1 kilometer",
                type: .runningDistance,
                goal: 1000,
                reward: Reward(
                    trophy: .silverMedal,
                    points: 5
                )
            ),
            Goal(
                id: "1005",
                title: "Quick Run",
                description: "Burn that donut by running 1 kilometer",
                type: .runningDistance,
                goal: 1000,
                reward: Reward(
                    trophy: .silverMedal,
                    points: 5
                )
            )
        ])
    }
}
