//
//  GoalParseTest.swift
//  ObjetivoTests
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import XCTest
@testable import Objetivo

class GoalParseTest: XCTestCase {

    var json: String!
    var expected = Goal(
        id: "1004",
        title: "Quick Run",
        description: "Burn that donut by running 1 kilometer",
        type: .runningDistance,
        goal: 1000,
        reward: Reward(
            trophy: .silverMedal,
            points: 5
        )
    )
    
    override func setUp() {
        json = """
         {
            "id": "1004",
            "title": "Quick Run",
            "description": "Burn that donut by running 1 kilometer",
            "type": "running_distance",
            "goal": 1000,
            "reward": {
                "trophy": "silver_medal",
                "points": 5
            }
         }
        """
    }

    func testParse() {
        let parseResult = try! JSONDecoder().decode(Goal.self, from: json.data(using: .utf8)!)
        XCTAssertEqual(expected.title, parseResult.title)
        XCTAssertEqual(expected.reward.points, parseResult.reward.points)
    }
}
