//
//  HttpNetworkingTest.swift
//  ObjetivoTests
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import XCTest
import RxTest
import RxBlocking
@testable import Objetivo

class HttpNetworkingTest: XCTestCase {

    func testGet() {
        let httpNetworking = HttpNetworking()
        do {
            let result = try httpNetworking.GET(of: "https://jsonplaceholder.typicode.com/posts/1").toBlocking().first()!
            let content = String(data: result.data!, encoding: .utf8)!
            XCTAssert(content.contains("\"userId\": 1,"))
        } catch {
            fatalError("Networking not available")
        }
    }
}
