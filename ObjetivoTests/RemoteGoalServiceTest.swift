//
//  RemoteGoalServiceTest.swift
//  ObjetivoTests
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import XCTest
import RxTest
import RxSwift
import RxBlocking
@testable import Objetivo

class RemoteGoalServiceTest: XCTestCase {
    
    var service: RemoteGoalService!
    var networking: Networking!
    
    override func setUp() {
        networking = MockNetworking()
        service = RemoteGoalService(networking: networking, baseApiUrl: "")
    }

    func testFetch() {
        do {
            let result = try service.fetch().toBlocking().first()
            XCTAssert(result?.count == 2)
        } catch { }
    }
}

fileprivate class MockNetworking: Networking {
    
    let json = """
        {
           "items":[
              {
                 "id":"1000",
                 "title":"Easy walk steps",
                 "description":"Walk 500 steps a day",
                 "type":"step",
                 "goal":500,
                 "reward":{
                    "trophy":"bronze_medal",
                    "points":5
                 }
              },
              {
                 "id":"1001",
                 "title":"Medium walk steps",
                 "description":"Walk 1000 steps a day",
                 "type":"step",
                 "goal":1000,
                 "reward":{
                    "trophy":"silver_medal",
                    "points":10
                 }
              }
           ]
        }
    """
    
    func GET(of url: String) -> Observable<NetworkingResponse> {
        return Observable.of(
            NetworkingResponseWithDataOnly(json.data(using: .utf8))
        )
    }
    
    func NetworkingResponseWithDataOnly(_ data: Data?) -> NetworkingResponse {
        return NetworkingResponse(request: nil, response: nil, data: data, error: nil)
    }
}
