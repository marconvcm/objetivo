//
//  LocalGoalServiceTest.swift
//  ObjetivoTests
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import XCTest
import RxTest
import RxSwift
import RxBlocking
@testable import Objetivo

class LocalGoalServiceTest: XCTestCase {

    var input = [
        Goal(
            id: "1004",
            title: "Quick Run",
            description: "Burn that donut by running 1 kilometer",
            type: .runningDistance,
            goal: 1000,
            reward: Reward(
                trophy: .silverMedal,
                points: 5
            )
        ),
        Goal(
            id: "1005",
            title: "Quick Run",
            description: "Burn that donut by running 1 kilometer",
            type: .runningDistance,
            goal: 1000,
            reward: Reward(
                trophy: .silverMedal,
                points: 5
            )
        )
    ]
    
    var service: LocalGoalService!
    
    override func setUp() {
        service = LocalGoalService()
    }
   
    func testSave() {
        do {
            let _ = try service.save(goals: input).toBlocking().first()
            let data = try service.fetch().toBlocking().first()
            XCTAssert(data?.count == 2)
        } catch { }
    }
}
