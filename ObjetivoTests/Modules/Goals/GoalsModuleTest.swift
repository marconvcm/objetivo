//
//  GoalsModuleTest.swift
//  ObjetivoTests
//
//  Created by Marcos Vinicius on 19/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import XCTest
import RxTest
import RxSwift
import RxBlocking
@testable import Objetivo

class GoalsModuleTest: XCTestCase {

    func testWireframe() {
        let repository = MockRepository()
        let module = GoalsModule(repository: repository)
        let view = module.wireframe()

        XCTAssert(view is GoalsViewInterface)
    }
}

fileprivate class MockRepository: GoalRepository {
    
    func fetch() -> Observable<[Goal]> { fatalError() }
}
