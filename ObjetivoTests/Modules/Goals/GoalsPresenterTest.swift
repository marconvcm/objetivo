//
//  GoalsPresenterTest.swift
//  ObjetivoTests
//
//  Created by Marcos Vinicius on 20/02/19.
//  Copyright © 2019 marconvcm. All rights reserved.
//

import XCTest
import RxTest
import RxSwift
import RxBlocking
@testable import Objetivo

class GoalsPresenterTest: XCTestCase, GoalsViewInterface {
    
    var presenter: GoalsPresenterInterface!
    
    func testRequestDataLoad() {
        let repository = MockRepository()
        let presenter = GoalsPresenter(view: self, repository: repository)
        presenter.requestDataLoad()
    }
    
    func renderData(with observable: Observable<[Goal]>) {
        do {
            let result = try observable.toBlocking().first()
            XCTAssert(result?.count == 2)
        } catch { }
    }
}

fileprivate class MockRepository: GoalRepository {
    
    func fetch() -> Observable<[Goal]> {
        return Observable.of([
            Goal(
                id: "1004",
                title: "Quick Run",
                description: "Burn that donut by running 1 kilometer",
                type: .runningDistance,
                goal: 1000,
                reward: Reward(
                    trophy: .silverMedal,
                    points: 5
                )
            ),
            Goal(
                id: "1005",
                title: "Quick Run",
                description: "Burn that donut by running 1 kilometer",
                type: .runningDistance,
                goal: 1000,
                reward: Reward(
                    trophy: .silverMedal,
                    points: 5
                )
            )
        ])
    }
}
